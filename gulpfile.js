/* GULP PLUGINS */

var gulp = require('gulp'),                          // Task runner
    babel = require("gulp-babel"),
    imagemin = require('gulp-imagemin'),             // Optimize images
    pngquant = require('imagemin-pngquant'),         // PNG plugin for ImageMin
    size = require('gulp-size'),                     // Display the size of something
    processHtml = require('gulp-processhtml'),       // Plugin uses Denis Ciccale's node-htmlprocessor to                                                          process/transform html files
    less = require('gulp-less'),                     // Compile Less to CSS
    lessReporter = require('gulp-less-reporter'),    // Error reporter for gulp-less
    autoprefixer = require('gulp-autoprefixer'),     // Prefix CSS
    minifycss = require('gulp-minify-css'),          // Minify CSS
    cssbeautify = require('gulp-cssbeautify'),       // Analog of the css-comb
    rename = require("gulp-rename"),                 // Rename files
    sourcemaps = require('gulp-sourcemaps'),         // Write source maps
    browserSync = require('browser-sync').create(),  // Synchronised browser testing
    streamqueue = require('streamqueue'),            // Pipe queued streams progressively, keeping datas                                                           order.
    svgmin = require('gulp-svgmin'), 				 // Minify SVG with SVGO
    rigger = require('gulp-rigger'),                 // Include content of one file to another
    jshint = require('gulp-jshint'),                 // JS code linter
    stylish = require('jshint-stylish'),             // Reporter for JSHint
    uglify = require('gulp-uglify'),                 // Minify JS
    concat = require('gulp-concat'),                 // Concatenates files
    runSequence = require('run-sequence').use(gulp), // Run a series of dependent gulp tasks in order
    del = require('del'),                            // Delete something
    ghPages = require('gulp-gh-pages'),              // Publish contents to Github pages
    watch = require('gulp-watch'),                   // Watch, that actually is an endless stream
    path = require('path');


/* Paths */
var projectPath = {
    src: {
        fonts: 'src/fonts/**/*.*',
        html: ['src/*.html', '!src/_*.html'],
        img: 'src/img/images/**/*.*',
        svg: 'src/img/svg/**/*.svg',
        jsVendor: 'src/js/vendor.js',
        jsCustom: 'src/js/index.js',
        spritesPNG: 'src/img/sprites/png/**/*.*',
        less: 'src/styles/main.less'
        // scss: 'src/styles/style.scss'
    },
    build: {
        fonts: 'build/fonts/',
        html: 'build/',
        img: 'build/img/images/',
        svg: 'build/img/svg/',
        js: 'build/js/',
        jsFile: 'main.js',
        spritesPNG: 'build/img/sprites/png/',
        styles: 'build/styles/'
    },
    watch: {
        fonts: 'src/fonts/**/*.*',
        html: 'src/**/*.html',
        img: 'src/img/images/**/*.*',
        js: 'src/js/**/*.js',
        spritesPNG: 'src/img/sprites/png/**/*.*',
        less: 'src/styles/**/*.less',
        scss: 'src/styles/**/*.scss'
    },
    clean: ['build/**/*', '!build/.gitignore'],
    ghPages: 'build/**/*'
};

/* TASKS */
/* BrowserSync */
gulp.task('brSynch', function () {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });
});

/* Clean */
gulp.task('clean', function () {
    del(projectPath.clean);
});

/* JavaScript */
gulp.task('js', function () {
    // return streamqueue(
    //     {objectMode: true},
    //     gulp.src(projectPath.src.jsVendor)
    //     .pipe(rigger())
    //     .pipe(jshint.reporter(stylish)),
    //     gulp.src(projectPath.src.jsCustom)
    //     .pipe(rigger())
    //     .pipe(jshint())
    //     .pipe(jshint.reporter(stylish))
    // )

    return gulp.src(projectPath.src.jsCustom)
    // .pipe(rigger())
    .pipe(babel())
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))

    // .pipe(concat(projectPath.build.jsFile))
    .pipe(sourcemaps.init())
    .pipe(gulp.dest(projectPath.build.js))
    // .pipe(rename({suffix: '.min'}))
    // .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(size({
        title: 'JS'
    }))
    .pipe(gulp.dest(projectPath.build.js))
    .pipe(browserSync.stream());
});

/* LESS */
gulp.task('less', function () {
    return gulp.src(projectPath.src.less)
    .pipe(sourcemaps.init())
    .pipe(less({
        paths: [path.join(__dirname, 'less', 'includes')]
    }))
    .on('error', lessReporter)
    .pipe(autoprefixer([
        'Android 2.3',
        'Android >= 4',
        'Chrome >= 20',
        'Firefox >= 24', // Firefox 24 is the latest ESR
        'Explorer >= 8',
        'iOS >= 6',
        'Opera >= 12',
        'Safari >= 6'
    ]))
    .pipe(cssbeautify({
        indent: '  ',
        openbrace: 'end-of-line',
        autosemicolon: true
    }))
    // .pipe(gulp.dest(projectPath.build.styles))
    // .pipe(rename({suffix: '.min'}))
    // .pipe(minifycss())
    // .pipe(sourcemaps.write('./'))
    .pipe(size({
        title: 'LESS'
    }))
    .pipe(gulp.dest(projectPath.build.styles))
    .pipe(browserSync.stream());
});

/* HTML */
gulp.task('html', function () {
    return gulp.src(projectPath.src.html)
    .pipe(processHtml({
        //recursive: true
    }))
    .pipe(size({
        title: 'Html'
    }))
    .pipe(gulp.dest(projectPath.build.html))
    .pipe(browserSync.stream());
});

/* Images */
gulp.task('images', function () {
    return gulp.src(projectPath.src.img)
    // .pipe(imagemin({
    //     progressive: true,
    //     optimizationLevel: 5,
        // use: [pngquant()],
    //     interlaced: true
    // }))
    .pipe(size({
        title: 'Images'
    }))
    .pipe(gulp.dest(projectPath.build.img))
    .pipe(browserSync.stream());
});

/* SVG */
// gulp.task('svg', function () {
//     return gulp.src(projectPath.src.svg)
//     .pipe(svgmin())
//     .pipe(size({
//         title: 'SVG'
//     }))
//     .pipe(gulp.dest(projectPath.build.svg))
//     .pipe(reload({stream: true}));
// });
/* /SVG */

/* Sprites - just rebase*/
// gulp.task('spritesPNG', function () {
//     return gulp.src(projectPath.src.spritesPNG)
//     .pipe(size({
//         title: 'sprites PNG'
//     }))
//     .pipe(gulp.dest(projectPath.build.spritesPNG))
//     .pipe(browserSync.stream());
// });

/* Fonts */
gulp.task('fonts', function () {
    return gulp.src(projectPath.src.fonts)
    .pipe(size({
        title: 'fonts'
    }))
    .pipe(gulp.dest(projectPath.build.fonts))
    .pipe(browserSync.stream());
});

/* Gh-pages - for github */
gulp.task('deploy', function () {
    return gulp.src(projectPath.ghPages)
    .pipe(ghPages());
});

/* Build */
gulp.task('build', function (callback) {
    runSequence(
        'clean',
        'js',
        'less',
        //'scss',
        'html',
        'images',
        // 'spritesPNG',
        'fonts',
        // 'deploy',
        callback
    )
});

/* Watch */
gulp.task('watch', ['brSynch'], function () {
    watch([projectPath.watch.js], function () {
        gulp.start('js');
    });
    watch([projectPath.watch.less], function () {
        gulp.start('less');
    });
    watch([projectPath.watch.html], function () {
        gulp.start('html');
    });
    watch([projectPath.watch.img], function () {
        gulp.start('images');
    });
    watch([projectPath.watch.spritesPNG], function () {
        gulp.start('spritesPNG');
    });
    watch([projectPath.watch.fonts], function () {
        gulp.start('fonts');
    });
});

/* Default */
gulp.task('default', ['watch'], function () {

});

